//
//  TDCell.swift
//  TruthOrDare
//
//  Created by Rahul Manghnani on 5/1/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class TDCell: UITableViewCell {

    @IBOutlet weak var tdLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initializeCell(td: String, isTruth: Bool) {
        tdLabel.text = td
        if isTruth == true {
            containerView.layer.borderColor = UIColor(hexString: "689864")!.cgColor
            containerView.layer.borderWidth = 3.0
        } else {
            containerView.layer.borderColor = UIColor(hexString: "993303")!.cgColor
            containerView.layer.borderWidth = 3.0
        }
    }
}
