//
//  CardView.swift
//  ZLSwipeableViewSwiftDemo
//
//  Created by Zhixuan Lai on 5/24/15.
//  Copyright (c) 2015 Zhixuan Lai. All rights reserved.
//

import UIKit

class CardView: UIView {

    @IBOutlet weak var tdLabel: UILabel!
    @IBOutlet weak var personsLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var overlayLabel: UILabel!
    
    var positiveFeedback = ["Awesome!", "You are the man/woman", "Well done!", "Everyone clap!", "Great job"]
    var negativeFeedback = ["Chicken!", "Coward!", "Go Home", "Disappointing", "Candy-ass!"]
    var currentPlayerId = ""
    var challenge = Challenge.truth
    var currentTDId = ""
    var td = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func setup() {
        // Shadow
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.25
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 4.0
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        // Corner Radius
        layer.cornerRadius = 10.0;
    }
    
    func initializeView(tdId: String, td: String, hideOverlay: Bool, challenge: Challenge, person: String? = nil) {
        self.challenge = challenge
        self.currentTDId = tdId
        tdLabel.text = td
        self.td = td
        if let p = person {
            personsLabel.text = "Completed successfully by " + p
        } else {
            personsLabel.text = ""
        }
        bgView.isHidden = hideOverlay
    }
    
    func setupFeedback(done: Bool, nextPlayer: String) {
        var feedback = ""
        if done == true {
            let randomIndex = Int(arc4random_uniform(UInt32(positiveFeedback.count)))
            feedback = positiveFeedback[randomIndex]
        } else {
            let randomIndex = Int(arc4random_uniform(UInt32(negativeFeedback.count)))
            feedback = negativeFeedback[randomIndex]
        }
        let color = done ? UIColor(hexString: "689864") : UIColor(hexString: "993303")
        let attrs1 = [NSAttributedStringKey.font : UIFont(name: "JosefinSans-Bold", size: 32), NSAttributedStringKey.foregroundColor : color]
        let attrs2 = [NSAttributedStringKey.font : UIFont(name: "JosefinSans-Bold", size: 32), NSAttributedStringKey.foregroundColor : UIColor.tdTextColor]
        let attrs3 = [NSAttributedStringKey.font : UIFont(name: "JosefinSans-Bold", size: 32), NSAttributedStringKey.foregroundColor : UIColor.tdTextColor]
        let attributedString1 = NSMutableAttributedString(string: feedback + "\n\n", attributes: attrs1)
        let attributedString2 = NSMutableAttributedString(string: "Choose from one of the options below to continue", attributes: attrs2)
        let attributedString3 = NSMutableAttributedString(string: "\n\n" + nextPlayer + "'s turn next", attributes: attrs3)
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        overlayLabel.attributedText = attributedString1
        bgView.alpha = 0.0
        bgView.isHidden = false
        UIView.animate(withDuration: 0.4) {
            self.bgView.alpha = 0.9
        }
    }
}
