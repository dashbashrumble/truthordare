//
//  StatsCell.swift
//  TruthOrDare
//
//  Created by Rahul Manghnani on 4/23/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class StatsCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var truthScoreLabel: UILabel!
    @IBOutlet weak var dareScoreLabel: UILabel!
    @IBOutlet weak var highsImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var highsImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewsSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondImageViewLeadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.contentView.layer.shadowOffset = CGSize(width: 8.0, height: 8.0)
        self.contentView.layer.shadowColor = UIColor.black.cgColor
        self.contentView.layer.shadowRadius = 4
        self.contentView.layer.shadowOpacity = 0.25
        self.contentView.layer.masksToBounds = false;
        self.contentView.clipsToBounds = false;
    }
    
    func initializeView(name: String, truthScoreText: String, dareScoreText: String, isSaint: Bool, isDaredevil: Bool, isZero: Bool) {
        nameLabel.text = name
        truthScoreLabel.text = "T's: " + truthScoreText
        dareScoreLabel.text = "D's: " + dareScoreText
        if isZero == true {
            highsImageViewWidthConstraint.constant = 0
            imageViewsSpaceConstraint.constant = 0
            secondImageViewWidthConstraint.constant = 40
            secondImageViewLeadingConstraint.constant = 16
            secondImageView.image = UIImage(named: "chicken")
        } else if isSaint == true && isDaredevil == true {
            highsImageViewWidthConstraint.constant = 40
            imageViewsSpaceConstraint.constant = 8
            highsImageView.image = UIImage(named: "saint")
            secondImageView.image = UIImage(named: "daredevil")
            secondImageViewWidthConstraint.constant = 40
            secondImageViewLeadingConstraint.constant = 16
        } else if isSaint == true && isDaredevil == false {
            highsImageViewWidthConstraint.constant = 0
            imageViewsSpaceConstraint.constant = 0
            secondImageView.image = UIImage(named: "saint")
            secondImageViewWidthConstraint.constant = 40
            secondImageViewLeadingConstraint.constant = 16
        } else if isSaint == false && isDaredevil == true {
            highsImageViewWidthConstraint.constant = 0
            imageViewsSpaceConstraint.constant = 0
            secondImageView.image = UIImage(named: "daredevil")
            secondImageViewWidthConstraint.constant = 40
            secondImageViewLeadingConstraint.constant = 16
        } else {
            highsImageViewWidthConstraint.constant = 0
            imageViewsSpaceConstraint.constant = 0
            secondImageViewWidthConstraint.constant = 0
            secondImageViewLeadingConstraint.constant = 0
        }
    }
}
