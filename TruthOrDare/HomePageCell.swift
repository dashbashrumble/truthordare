//
//  HomePageCell.swift
//  TruthOrDare
//
//  Created by Rahul Manghnani on 4/12/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class HomePageCell: UITableViewCell {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var coloredView: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.shadowView.layer.shadowOffset = CGSize(width: 8.0, height: 8.0)
        self.shadowView.layer.shadowColor = UIColor.black.cgColor
        self.shadowView.layer.shadowRadius = 4
        self.shadowView.layer.shadowOpacity = 0.25
        self.shadowView.layer.masksToBounds = false;
        self.shadowView.clipsToBounds = false;
    }
    
    func initializeView(icon: UIImage, color: UIColor, text: String, isEnabled: Bool) {
        iconImageView.image = icon
        coloredView.backgroundColor = color
        categoryLabel.text = text
        if isEnabled == true {
            containerView.alpha = 1.0
        } else {
            containerView.alpha = 0.5
        }
    }
}
