//
//  PlayerCell.swift
//  TruthOrDare
//
//  Created by Rahul Manghnani on 4/19/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import M13Checkbox

protocol PlayerDelegate: class {
    func selectPlayer(id: String)
}

enum HighsAndLows {
    case saint
    case daredevil
    case chicken
    case none
}

class PlayerCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var checkBox: M13Checkbox!
    @IBOutlet weak var truthsLabel: UILabel!
    @IBOutlet weak var daresLabel: UILabel!
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var firstImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewsSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondImageViewLeadingConstraint: NSLayoutConstraint!
    private var playerId: String!
    private weak var delegate: PlayerDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        self.contentView.layer.shadowOffset = CGSize(width: 8.0, height: 8.0)
        self.contentView.layer.shadowColor = UIColor.black.cgColor
        self.contentView.layer.shadowRadius = 4
        self.contentView.layer.shadowOpacity = 0.25
        self.contentView.layer.masksToBounds = false;
        self.contentView.clipsToBounds = false;
        
        checkBox.layer.cornerRadius = 15.0
    }
    
    @IBAction func valueChanged(_ sender: Any) {
        checkBox.isSelected = !checkBox.isSelected
//        checkBox.setCheckState(M13Checkbox.CheckState.checked, animated: true)
        delegate?.selectPlayer(id: playerId)
    }
    
    func initializeView(id: String, name: String, truths: String, dares: String, isNewlyAdded: Bool, delegate: PlayerDelegate, isSaint: Bool, isDaredevil: Bool, isZero: Bool, isSelected: Bool) {
        self.delegate = delegate
        playerId = id
        if isNewlyAdded == true || isSelected == true {
            checkBox.setCheckState(M13Checkbox.CheckState.checked, animated: true)
        } else {
            checkBox.setCheckState(M13Checkbox.CheckState.unchecked, animated: true)
        }
        nameLabel.text = name
        truthsLabel.text = "Truths: " + truths
        daresLabel.text = "Dares: " + dares
        
        if isNewlyAdded == true {
            firstImageViewWidthConstraint.constant = 0
            imageViewsSpaceConstraint.constant = 0
            secondImageViewWidthConstraint.constant = 0
            secondImageViewLeadingConstraint.constant = 0
            firstImageView.image = nil
            secondImageView.image = nil
        } else if isZero == true {
            firstImageViewWidthConstraint.constant = 0
            imageViewsSpaceConstraint.constant = 0
            secondImageViewWidthConstraint.constant = 40
            secondImageViewLeadingConstraint.constant = 16
            secondImageView.image = UIImage(named: "chicken")
        } else if isSaint == true && isDaredevil == true {
            firstImageViewWidthConstraint.constant = 40
            imageViewsSpaceConstraint.constant = 8
            firstImageView.image = UIImage(named: "saint")
            secondImageView.image = UIImage(named: "daredevil")
            secondImageViewWidthConstraint.constant = 40
            secondImageViewLeadingConstraint.constant = 16
        } else if isSaint == true && isDaredevil == false {
            firstImageViewWidthConstraint.constant = 0
            imageViewsSpaceConstraint.constant = 0
            secondImageView.image = UIImage(named: "saint")
            secondImageViewWidthConstraint.constant = 40
            secondImageViewLeadingConstraint.constant = 16
        } else if isSaint == false && isDaredevil == true {
            firstImageViewWidthConstraint.constant = 0
            imageViewsSpaceConstraint.constant = 0
            secondImageView.image = UIImage(named: "daredevil")
            secondImageViewWidthConstraint.constant = 40
            secondImageViewLeadingConstraint.constant = 16
        } else {
            firstImageViewWidthConstraint.constant = 0
            imageViewsSpaceConstraint.constant = 0
            secondImageViewWidthConstraint.constant = 0
            secondImageViewLeadingConstraint.constant = 0
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        checkBox.setCheckState(M13Checkbox.CheckState.unchecked, animated: false)
    }
}
