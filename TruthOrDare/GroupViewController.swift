//
//  GroupViewController.swift
//  TruthOrDare
//
//  Created by Rahul Manghnani on 4/18/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import M13Checkbox
import GoogleMobileAds

class GroupViewController: UIViewController {

    @IBOutlet weak var mainCheckBox: M13Checkbox!
    @IBOutlet weak var keepScoresLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalNumberLabel: UILabel!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var selectedLabel: UILabel!
    @IBOutlet weak var playerNameTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    
    private var newPlayerIds = Set<String>()
    private var absolutelyNewPlayerIds = Set<String>()
    private var saints = Set<String>()
    private var daredevils = Set<String>()
    private var truthEggs = Set<String>()
    private var dareEggs = Set<String>()
    var allPlayers = [String]()
    var allPlayerIds = [String]()
    var playerIds = [String : String]()
    var playerTruths = [String : [Int]]()
    var playerDares = [String : [Int]]()
    var category = Category.kids
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playerNameTextField.delegate = self
        tableView.register(UINib(nibName: "PlayerCell", bundle: Bundle.main), forCellReuseIdentifier: "PlayerCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = UIColor.clear
        
        mainCheckBox.layer.cornerRadius = 15.0
        keepScoresLabel.text = "Keep Scores"
        totalLabel.text = "Total Players:"
        totalNumberLabel.text = "0"
        
        let savedPlayers = AppProvider.userDefaults.object(forKey: Keys.playerIds) as? [String : String] ?? [String : String]()
        for (playerId, playerName) in savedPlayers {
            allPlayerIds.append(playerId)
            allPlayers.append(playerName)
        }
        
        if allPlayers.count == 0 {
            playerNameLabel.isHidden = true
            selectedLabel.isHidden = true
        } else {
            playerNameLabel.isHidden = false
            selectedLabel.isHidden = false
        }
        
        playerTruths = AppProvider.userDefaults.object(forKey: Keys.playerTruths) as? [String : [Int]] ?? [String : [Int]]()
        playerDares = AppProvider.userDefaults.object(forKey: Keys.playerDares) as? [String : [Int]] ?? [String : [Int]]()
        disableEverything()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        errorLabel.text = "Select at least 2 players to proceed"
        errorLabel.alpha = 0.0
        
        
        var truthMax = Int.min
        for (id, score) in playerTruths {
            if score[0] == 0 {
                truthEggs.insert(id)
            } else if score[0] > truthMax {
                truthMax = score[0]
                if score[0] > 0 {
                    saints.removeAll()
                    saints.insert(id)
                }
            } else if score[0] == truthMax {
                if score[0] > 0 {
                    saints.insert(id)
                }
            } 
        }
        
        var dareMax = Int.min
        for (id, score) in playerDares {
            if score[0] == 0 {
                dareEggs.insert(id)
            } else if score[0] > dareMax {
                dareMax = score[0]
                if score[0] > 0 {
                    daredevils.removeAll()
                    daredevils.insert(id)
                }
            } else if score[0] == dareMax {
                if score[0] > 0 {
                    daredevils.insert(id)
                }
            }
        }
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/4087578816"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddNewTDViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func addPlayer(_ sender: Any) {
        if playerNameTextField.text?.isEmpty == false {
            playerNameLabel.isHidden = false
            selectedLabel.isHidden = false
            let playerName = playerNameTextField.text!
            let playerId = randomString(length: 8)
            var savedPlayers = AppProvider.userDefaults.object(forKey: Keys.playerIds) as? [String : String] ?? [String : String]()
            savedPlayers[playerId] = playerName
            AppProvider.userDefaults.set(savedPlayers, forKey: Keys.playerIds)
            
            var savedTruths = AppProvider.userDefaults.object(forKey: Keys.playerTruths) as? [String : [Int]] ?? [String : [Int]]()
            savedTruths[playerId] = [Int](repeating: 0, count: 2)
            AppProvider.userDefaults.set(savedTruths, forKey: Keys.playerTruths)
            
            var savedDares = AppProvider.userDefaults.object(forKey: Keys.playerDares) as? [String : [Int]] ?? [String : [Int]]()
            savedDares[playerId] = [Int](repeating: 0, count: 2)
            AppProvider.userDefaults.set(savedDares, forKey: Keys.playerDares)
            
            allPlayerIds.insert(playerId, at: 0)
            allPlayers.insert(playerName, at: 0)
            newPlayerIds.insert(playerId)
            absolutelyNewPlayerIds.insert(playerId)
            totalNumberLabel.text = "\(newPlayerIds.count)"
            playerNameTextField.text = ""
            tableView.insertSections(IndexSet(integer: 0), with: UITableViewRowAnimation.automatic)
        }
    }
    
    @IBAction func startGame(_ sender: Any) {
        if mainCheckBox.checkState == M13Checkbox.CheckState.checked && newPlayerIds.count <= 1 {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
                self.errorLabel.alpha = 1.0
            }) { (finished) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                        self.errorLabel.alpha = 0.0
                    }, completion: nil)
                })
            }
        } else {
            performSegue(withIdentifier: "TDViewController", sender: nil)
        }
    }
    
    @IBAction func valueChanged(_ sender: M13Checkbox) {
        switch sender.checkState {
        case .unchecked:
            disableEverything()
        case .checked:
            tableView.alpha = 1.0
            tableView.isUserInteractionEnabled = true
            totalLabel.alpha = 1.0
            totalNumberLabel.alpha = 1.0
            playerNameLabel.alpha = 1.0
            selectedLabel.alpha = 1.0
            playerNameTextField.text = ""
            playerNameTextField.isUserInteractionEnabled = true
            playerNameTextField.isEnabled = true
            playerNameTextField.alpha = 1.0
            addButton.isEnabled = true
            addButton.isUserInteractionEnabled = true
            addButton.alpha = 1.0
        case .mixed:
            break
        }
    }
    
    private func disableEverything() {
        tableView.alpha = 0.3
        tableView.isUserInteractionEnabled = false
        totalLabel.alpha = 0.3
        totalNumberLabel.alpha = 0.3
        playerNameLabel.alpha = 0.3
        selectedLabel.alpha = 0.3
        playerNameTextField.text = ""
        playerNameTextField.isUserInteractionEnabled = false
        playerNameTextField.isEnabled = false
        playerNameTextField.alpha = 0.3
        addButton.isEnabled = false
        addButton.isUserInteractionEnabled = false
        addButton.alpha = 0.3
    }

    private func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "TDViewController":
            let vc = segue.destination as! TDViewController
            vc.category = self.category
            if mainCheckBox.checkState == M13Checkbox.CheckState.checked {
                vc.playerIds = Array(newPlayerIds)
            }
        default:
            return
        }
    }
}

extension GroupViewController: PlayerDelegate {
    func selectPlayer(id: String) {
        if newPlayerIds.contains(id) {
            newPlayerIds.remove(id)
        } else {
            newPlayerIds.insert(id)
        }
        totalNumberLabel.text = "\(newPlayerIds.count)"
    }
}

extension GroupViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return allPlayers.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell") as! PlayerCell
        let playerName = allPlayers[indexPath.section]
        let playerId = allPlayerIds[indexPath.section]
        let truthString = "\(playerTruths[playerId]?[0] ?? 0)/\(playerTruths[playerId]?[1] ?? 0)"
        let dareString = "\(playerDares[playerId]?[0] ?? 0)/\(playerDares[playerId]?[1] ?? 0)"
        cell.initializeView(id: playerId, name: playerName, truths: truthString, dares: dareString, isNewlyAdded: absolutelyNewPlayerIds.contains(playerId), delegate: self, isSaint: saints.contains(playerId), isDaredevil: daredevils.contains(playerId), isZero: (truthEggs.contains(playerId) && dareEggs.contains(playerId)), isSelected: newPlayerIds.contains(playerId))
        return cell
    }
}

extension GroupViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
}

extension GroupViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

extension GroupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
