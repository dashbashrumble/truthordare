//
//  StatsViewController.swift
//  TruthOrDare
//
//  Created by Rahul Manghnani on 4/23/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Crashlytics

class StatsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noStatsLabel: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var interstitial: GADInterstitial!

    var truthScores = [String : [Int]]()
    var dareScores = [String : [Int]]()
    var playerIds = [String]()
    var playerNames = [String]()
    var saints = Set<String>()
    var daredevils = Set<String>()
    var gameOn = false
    private var firstInterstitialShown = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interstitial = createAndLoadInterstitial()
        
        let playersDict = AppProvider.userDefaults.object(forKey: Keys.playerIds) as? [String : String] ?? [String : String]()
        
        var tempIds = Set<String>()
        for (id, _) in truthScores {
            tempIds.insert(id)
        }
        
        for (id, name) in playersDict {
            if tempIds.contains(id) == true {
                playerIds.append(id)
                playerNames.append(name)
            }
        }
        tableView.register(UINib(nibName: "StatsCell", bundle: Bundle.main), forCellReuseIdentifier: "StatsCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/3975898177"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
        
        var truthMax = Int.min
        for (id, score) in truthScores {
            if score[0] > truthMax {
                truthMax = score[0]
                if score[0] > 0 {
                    saints.removeAll()
                    saints.insert(id)
                }
            } else if score[0] == truthMax {
                if score[0] > 0 {
                    saints.insert(id)
                }
            }
        }
        
        var dareMax = Int.min
        for (id, score) in dareScores {
            if score[0] > dareMax {
                dareMax = score[0]
                if score[0] > 0 {
                    daredevils.removeAll()
                    daredevils.insert(id)
                }
            } else if score[0] == dareMax {
                if score[0] > 0 {
                    daredevils.insert(id)
                }
            }
        }
        
        if playerIds.count <= 0 {
            noStatsLabel.text = "No players statistics available."
            noStatsLabel.isHidden = false
        } else {
            noStatsLabel.text = nil
            noStatsLabel.isHidden = true
        }
        
        if gameOn == true {
            titleLabel.text = "Game Statistics"
            var vcs = navigationController?.viewControllers
            let count = vcs!.count
            vcs?.remove(at: count - 2)
            navigationController?.setViewControllers(vcs!, animated: true)
        } else {
            titleLabel.text = "All Time Statistics"
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: #selector(TDViewController.back))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if firstInterstitialShown == false {
            firstInterstitialShown = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                Answers.logCustomEvent(withName: "First Interstitial on Stats", customAttributes: nil)
                if self.interstitial.isReady {
                    self.interstitial.present(fromRootViewController: self)
                }
            }
        } else {
            Answers.logCustomEvent(withName: "Second Interstitial on Stats", customAttributes: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
                if self.interstitial.isReady {
                    self.interstitial.present(fromRootViewController: self)
                }
            }
        }
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9043117893678177/1095665536")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
}

extension StatsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
}

extension StatsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return playerNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatsCell") as! StatsCell
        let name = playerNames[indexPath.section]
        let id = playerIds[indexPath.section]
        let truthString = "\(truthScores[id]?[0] ?? 0)/\(truthScores[id]?[1] ?? 0)"
        let dareString = "\(dareScores[id]?[0] ?? 0)/\(dareScores[id]?[1] ?? 0)"
        var truths = 0
        var dares = 0
        if let score = truthScores[id] {
            truths = score[0]
        }
        if let score = dareScores[id] {
            dares = score[0]
        }
        cell.initializeView(name: name, truthScoreText: truthString, dareScoreText: dareString, isSaint: saints.contains(id), isDaredevil: daredevils.contains(id), isZero: ((truths == 0) && (dares == 0)))
        return cell
    }
}

extension StatsViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

extension StatsViewController: GADInterstitialDelegate {
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
}
