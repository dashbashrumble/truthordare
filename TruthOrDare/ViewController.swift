//
//  ViewController.swift
//  TruthOrDare
//
//  Created by Rahul Manghnani on 4/9/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Crashlytics

enum Category {
    case easy
    case hard
    case dirty
    case custom
    case crazy
    case soft
    case sexy
    case extreme
    case kids
    case teen
    case adult
    
    var category: String {
        switch self {
        case .easy:
            return "Easy"
        case .hard:
            return "Hard"
        case .dirty:
            return "Dirty"
        case .custom:
            return "Custom"
        case .crazy:
            return "Crazy"
        case .soft:
            return "Soft"
        case .sexy:
            return "Sexy"
        case .extreme:
            return "Extreme"
        case .kids:
            return "Kids"
        case .teen:
            return "Teen"
        case .adult:
            return "Adult"
        }
    }
    
    var icon: UIImage {
        switch self {
        case .easy:
            return UIImage(named: "td_easy")!
        case .hard:
            return UIImage(named: "td_hard")!
        case .dirty:
            return UIImage(named: "td_adult")!
        case .custom:
            return UIImage(named: "td_custom")!
        case .crazy:
            return UIImage(named: "td_crazy")!
        case .soft:
            return UIImage(named: "td_soft")!
        case .sexy:
            return UIImage(named: "td_sexy")!
        case .extreme:
            return UIImage(named: "td_extreme")!
        case .kids:
            return UIImage(named: "td_kids")!
        case .teen:
            return UIImage(named: "td_teen")!
        case .adult:
            return UIImage(named: "td_adult")!
        }
    }
    
    var color: UIColor {
        switch self {
        case .easy:
            return UIColor(hexString: "E65262")!
        case .hard:
            return UIColor(hexString: "F36B4F")!
        case .dirty:
            return UIColor(hexString: "F7C853")!
        case .custom:
            return UIColor(hexString: "9BCD65")!
        case .crazy:
            return UIColor(hexString: "4CBAE1")!
        case .soft:
            return UIColor(hexString: "5A97E5")!
        case .sexy:
            return UIColor(hexString: "46C9A9")!
        case .extreme:
            return UIColor(hexString: "A78DE5")!
        case .kids:
            return UIColor(hexString: "E583B9")!
        case .teen:
            return UIColor(hexString: "83B0AD")!
        case .adult:
            return UIColor(hexString: "933102")!
        }
    }
    
    var truthDict: [String : String] {
        switch self {
        case .custom:
            return AppProvider.userDefaults.object(forKey: Keys.customTruths) as? [String: String] ?? [String : String]()
        case .kids:
            return Bank.kidTs
        case .teen:
            return Bank.teenTs
        case .dirty:
            return Bank.dirtyTs
        case .crazy:
            return Bank.crTs
        case .easy:
            return Bank.doableTs
        case .hard:
            return Bank.hardTs
        default:
            return [String : String]()
        }
    }
    
    var dareDict: [String : String] {
        switch self {
        case .custom:
            return AppProvider.userDefaults.object(forKey: Keys.customDares) as? [String: String] ?? [String : String]()
        case .kids:
            return Bank.kidDs
        case .teen:
            return Bank.teenDs
        case .dirty:
            return Bank.dirtyDs
        case .crazy:
            return Bank.crDs
        case .easy:
            return Bank.doableDs
        case .hard:
            return Bank.hardDs
        default:
            return [String : String]()
        }
    }
    
    var truthDataSource: [String] {
        switch self {
        case .custom:
            var truths = [String]()
            let customTruths = AppProvider.userDefaults.object(forKey: Keys.customTruths) as? [String: String] ?? [String : String]()
            for (id, _) in customTruths {
                truths.append(id)
            }
            return truths
        default:
            var truths = [String]()
            for (id, _) in self.truthDict {
                truths.append(id)
            }
            return truths
        }
    }
    
    var dareDataSource: [String] {
        switch self {
        case .custom:
            var dares = [String]()
            let customDares = AppProvider.userDefaults.object(forKey: Keys.customDares) as? [String: String] ?? [String : String]()
            for (id, _) in customDares {
                dares.append(id)
            }
            return dares
        default:
            var dares = [String]()
            for (id, _) in self.dareDict {
                dares.append(id)
            }
            return dares
        }
    }
    
    var isEnabled: Bool {
        if self.truthDataSource.count == 0 && self.dareDataSource.count == 0 {
            return false
        }
        return true
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var pitchforkTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var statsButton: UIButton!
    @IBOutlet weak var addCardButton: UIButton!
    var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
    var localTimeZoneName: String { return TimeZone.current.identifier }
    var systemVersion: String { return UIDevice.current.systemVersion }
    
    private var layoutDoneOnce = false
    var cards: [Category] = [.easy, .hard, .teen, .custom, .kids, .crazy, .dirty]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoLabel.alpha = 0.0
        tableView.backgroundColor = UIColor.clear
        tableView.alpha = 0.0
        statsButton.alpha = 0.0
        addCardButton.alpha = 0.0
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "HomePageCell", bundle: Bundle.main), forCellReuseIdentifier: "HomePageCell")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: (self.cards.count - 1)), at: UITableViewScrollPosition.bottom, animated: false)
        bannerView.adUnitID = "ca-app-pub-9043117893678177/5954492580"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
        
        let topBarHeight = (self.navigationController?.navigationBar.frame.height ?? 0.0)
        pitchforkTopConstraint.constant = 263.5 - topBarHeight
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.reloadCustomCell), name: NSNotification.Name(rawValue: Keys.reloadCustomCell), object: nil)
        
        if AppProvider.userDefaults.object(forKey: Keys.launchEvents) == nil {
            AppProvider.userDefaults.set("Events sent", forKey: Keys.launchEvents)
            Answers.logCustomEvent(withName: "Launch",
                                   customAttributes: [
                                    "Time": getTodayString(),
                                    "Time Zone Abbreviation": localTimeZoneAbbreviation,
                                    "Time Zone Name": localTimeZoneName,
                                    "OS Version": systemVersion
                ])
            if let countryCode = NSLocale.current.regionCode {
                Answers.logCustomEvent(withName: "Country",
                                       customAttributes: [
                                        "Country Code": countryCode
                    ])
            }
        }
        
        if let countryCode = NSLocale.current.regionCode {
            Answers.logCustomEvent(withName: "Country",
                                   customAttributes: [
                                    "Country Code": countryCode
                ])
        }
    }
    
    @objc func reloadCustomCell() {
        tableView.reloadSections(IndexSet(integer: 3), with: .none)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if layoutDoneOnce == false {
            layoutDoneOnce = true
            pitchforkTopConstraint.constant = -300
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                UIView.animate(withDuration: 0.5, animations: {
                    self.logoLabel.alpha = 1.0
                    self.tableView.alpha = 1.0
                    self.statsButton.alpha = 1.0
                    self.addCardButton.alpha = 1.0
                }, completion: { (finished) in
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                })
            }
            UIView.animate(withDuration: 1.5, delay: 0.5, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.4, options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
            }) { (finished) in
                
            }
        }
    }
    
    @IBAction func showStats(_ sender: Any) {
        performSegue(withIdentifier: "ShowStats", sender: nil)
    }

    @IBAction func addNewCard(_ sender: Any) {
        performSegue(withIdentifier: "AddNewTDViewController", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "GroupViewController":
            let vc = segue.destination as! GroupViewController
            vc.category = sender as! Category
        case "ShowStats":
            let vc = segue.destination as! StatsViewController
            vc.truthScores = AppProvider.userDefaults.object(forKey: Keys.playerTruths) as? [String : [Int]] ?? [String : [Int]]()
            vc.dareScores = AppProvider.userDefaults.object(forKey: Keys.playerDares) as? [String : [Int]] ?? [String : [Int]]()
        default:
            return
        }
    }
    
    func getTodayString() -> String{
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(year!) + "-" + String(month!) + "-" + String(day!) + " " + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomePageCell") as! HomePageCell
        let card = cards[indexPath.section]
        cell.initializeView(icon: card.icon, color: card.color, text: card.category, isEnabled: card.isEnabled)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let card = cards[indexPath.section]
        guard card.isEnabled == true else { return }
        Answers.logCustomEvent(withName: "Selected Category",
                               customAttributes: [
                                "Category": card.category
            ])
        self.performSegue(withIdentifier: "GroupViewController", sender: card)
    }
}

extension ViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}


