//
//  AddNewTDViewController.swift
//  TruthOrDare
//
//  Created by Rahul Manghnani on 5/1/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AddNewTDViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var noCardsLabel: UILabel!
    
    private var cards = [String]()
    private var cardIds = [String]()
    private var truthIds = Set<String>()
    private var dareIds = Set<String>()
    
    var interstitial: GADInterstitial!
    
    var addedNewCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interstitial = createAndLoadInterstitial()
        textView.delegate = self
        
        tableView.backgroundColor = UIColor.clear
        tableView.register(UINib(nibName: "TDCell", bundle: Bundle.main), forCellReuseIdentifier: "TDCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        let customTruths = AppProvider.userDefaults.object(forKey: Keys.customTruths) as? [String: String] ?? [String : String]()
        for (id, t) in customTruths {
            cardIds.append(id)
            cards.append(t)
            truthIds.insert(id)
        }
        
        let customDares = AppProvider.userDefaults.object(forKey: Keys.customDares) as? [String: String] ?? [String : String]()
        for (id, d) in customDares {
            cardIds.append(id)
            cards.append(d)
            dareIds.insert(id)
        }
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/2148892757"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
        
        errorLabel.text = nil
        errorLabel.alpha = 0.0
        noCardsLabel.text = "You haven't added any cards yet."
        hideUnhideStuff()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddNewTDViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func hideUnhideStuff() {
        if cards.count > 0 {
            noCardsLabel.isHidden = true
            tableView.isHidden = false
        } else {
            noCardsLabel.isHidden = false
            tableView.isHidden = true
        }
    }
    
    @IBAction func valueChanged(_ sender: Any) {
        
    }
    
    @IBAction func addCard(_ sender: Any) {
        if textView.text.isEmpty == true {
            showError(error: "Please enter a valid text")
        } else {
            let cardId = randomString(length: 8)
            let card = textView.text!
            let prevCount = cards.count
            cards.insert(card, at: 0)
            cardIds.insert(cardId, at: 0)
            if segmentedControl.selectedSegmentIndex == 0 {   // Truth
                truthIds.insert(cardId)
                var customTruths = AppProvider.userDefaults.object(forKey: Keys.customTruths) as? [String: String] ?? [String : String]()
                customTruths[cardId] = card
                AppProvider.userDefaults.set(customTruths, forKey: Keys.customTruths)
            } else {
                dareIds.insert(cardId)
                var customDares = AppProvider.userDefaults.object(forKey: Keys.customDares) as? [String: String] ?? [String : String]()
                customDares[cardId] = card
                AppProvider.userDefaults.set(customDares, forKey: Keys.customDares)
            }
            hideUnhideStuff()
            tableView.insertSections(IndexSet(integer: 0), with: UITableViewRowAnimation.automatic)
            textView.text = ""
            showError(error: "Added successfully", isError: false)
            addedNewCount += 1
            if addedNewCount == 2 {
                addedNewCount = 0
                if interstitial.isReady {
                    interstitial.present(fromRootViewController: self)
                }
            }
            if prevCount == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: Keys.reloadCustomCell), object: nil)
            }
        }
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9043117893678177/2108282733")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func showError(error: String, isError: Bool = true) {
        if isError == true {
            errorLabel.textColor = UIColor(hexString: "C70039")!
        } else {
            errorLabel.textColor = UIColor(hexString: "FFD111")!
        }
        errorLabel.text = error
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.errorLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.errorLabel.alpha = 0.0
                    self.errorLabel.text = nil
                }, completion: nil)
            })
        }
    }
    
    private func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
}

extension AddNewTDViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
}

extension AddNewTDViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TDCell") as! TDCell
        let card = cards[indexPath.section]
        cell.initializeCell(td: card, isTruth: truthIds.contains(cardIds[indexPath.section]))
        return cell
    }
}

extension AddNewTDViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

extension AddNewTDViewController: GADInterstitialDelegate {
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
}

extension AddNewTDViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
