//
//  TDViewController.swift
//  TruthOrDare
//
//  Created by Rahul Manghnani on 4/13/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import ZLSwipeableViewSwift
import Cartography
import GameKit
import SCLAlertView
import GoogleMobileAds
import Crashlytics

func randomBetween(min: Int, max: Int) -> Int {
    return GKRandomSource.sharedRandom().nextInt(upperBound: max - min) + min
}

enum Challenge {
    case truth
    case dare
}

enum DisplayState {
    case showCard
    case doIt
}

class Score {
    var done = 0
    var total = 0
    
    init(done: Int = 0, total: Int = 0) {
        self.done = done
        self.total = total
    }
}

class TDViewController: UIViewController {

    var swipeableView: ZLSwipeableView!
    var category = Category.kids
    var playerIds = [String]()
    private var playerNames = [String]()
    private var customGame = false
    private var truthIds = [String]()
    private var dareIds = [String]()
    private var truths = [String]()
    private var dares = [String]()
    private var gameStarted = false
    private var challenge = Challenge.truth
    private var totalNumberOfTruthsAndDaresDisplayed = 0
    var displayState = DisplayState.showCard
    private var randomTruthId = ""
    private var randomTruth: String {
        let randomIndex = Int(arc4random_uniform(UInt32(truths.count)))
        randomTruthId = truthIds[randomIndex]
        return truths[randomIndex]
    }
    private var randomDareId = ""
    private var randomDare: String {
        let randomIndex = Int(arc4random_uniform(UInt32(dares.count)))
        randomDareId = dareIds[randomIndex]
        return dares[randomIndex]
    }
    
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet weak var truthButton: UIButton!
    @IBOutlet weak var dareButton: UIButton!
    @IBOutlet weak var randomButton: UIButton!
    @IBOutlet weak var truthView: UIView!
    @IBOutlet weak var randomView: UIView!
    @IBOutlet weak var dareView: UIView!
    @IBOutlet weak var truthScoreLabel: UILabel!
    @IBOutlet weak var truthScoreValueLabel: UILabel!
    @IBOutlet weak var dareScoreLabel: UILabel!
    @IBOutlet weak var dareScoreValueLabel: UILabel!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var bannerView: GADBannerView!
    
    private var playersDictionary = [String :  String]()
    private var truthScores = [String : [Int]]()
    private var dareScores = [String : [Int]]()
    var contentView: CardView!
    var noTruths  = false
    var noDares = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        truthIds = category.truthDataSource
        dareIds = category.dareDataSource
        for id in truthIds {
            truths.append(category.truthDict[id] ?? "")
        }
        for id in dareIds {
            dares.append(category.dareDict[id] ?? "")
        }
        
        noTruths = !(truthIds.count > 0)
        noDares = !(dareIds.count > 0)
        
        customGame = (playerIds.count > 0)
        
        playersDictionary = AppProvider.userDefaults.object(forKey: Keys.playerIds) as? [String : String] ?? [String : String]()
        for id in playerIds {
            playerNames.append(playersDictionary[id] ?? "")
        }
        
        addShadow(to: truthView)
        addShadow(to: randomView)
        addShadow(to: dareView)
        addRoundedCorners(to: truthButton)
        addRoundedCorners(to: randomButton)
        addRoundedCorners(to: dareButton)
        
        var vcs = navigationController?.viewControllers
        let count = vcs!.count
        vcs?.remove(at: count - 2)
        navigationController?.setViewControllers(vcs!, animated: true)
        
        swipeableView = ZLSwipeableView()
        swipeableView.numberOfActiveView = 1
        swipeableView.isUserInteractionEnabled = false
        view.addSubview(swipeableView)
        swipeableView.didStart = {view, location in
//            print("Did start swiping view at location: \(location)")
        }
        swipeableView.swiping = {view, location, translation in
//            print("Swiping at view location: \(location) translation: \(translation)")
        }
        swipeableView.didEnd = {view, location in
//            print("Did end swiping view at location: \(location)")
        }
        swipeableView.didSwipe = {view, direction, vector in
//            print("Did swipe view in direction: \(direction), vector: \(vector)")
        }
        swipeableView.didCancel = {view in
//            print("Did cancel swiping view")
        }
        swipeableView.didTap = {view, location in
//            print("Did tap at location \(location)")
        }
        swipeableView.didDisappear = { view in
//            print("Did disappear swiping view")
        }
        
        let window = UIApplication.shared.keyWindow
        var topPadding: CGFloat! = 0
        if #available(iOS 11.0, *) {
            topPadding = window?.safeAreaInsets.top
        } else {
            // Fallback on earlier versions
        }
        var bottomPadding: CGFloat! = 0
        if #available(iOS 11.0, *) {
            bottomPadding = window?.safeAreaInsets.bottom
        } else {
            // Fallback on earlier versions
        }
        
        constrain(swipeableView, view) { view1, view2 in
            view1.left == view2.left+50
            view1.right == view2.right-50
            view1.top == view2.top + 120 + topPadding
            view1.bottom == view2.bottom - 220 - bottomPadding
        }
        
        for id in playerIds {
            truthScores[id] = [Int](repeating: 0, count: 2)
            dareScores[id] = [Int](repeating: 0, count: 2)
        }
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/8737270269"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
        
        truthScoreLabel.text = "Truths"
        truthScoreValueLabel.text = "0/0"
        dareScoreLabel.text = "Dares"
        dareScoreValueLabel.text = "0/0"
        truthScoreLabel.isHidden = true
        truthScoreValueLabel.isHidden = true
        dareScoreLabel.isHidden = true
        dareScoreValueLabel.isHidden = true
        finishButton.isHidden = true
        hideTDButtonsIfEmpty()
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Quit", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TDViewController.back))
        newBackButton.setTitleTextAttributes([NSAttributedStringKey.font : UIFont(name: "JosefinSans-Bold", size: 20)!], for: UIControlState.normal)
        newBackButton.tintColor = UIColor(hexString: "FFD111")!
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
        showQuitGameAlert()
    }
    
    @IBAction func finishGame(_ sender: Any) {
        showFinishGameAlert()
    }
    
    private func finishGameAndOpenStats() {
        var savedTruths = AppProvider.userDefaults.object(forKey: Keys.playerTruths) as? [String : [Int]] ?? [String : [Int]]()
        for (playerId, score) in truthScores {
            if var savedScore = savedTruths[playerId] {
                savedScore[0] += score[0]
                savedScore[1] += score[1]
                savedTruths[playerId] = savedScore
            } else {
                savedTruths[playerId] = [score[0], score[1]]
            }
        }
        AppProvider.userDefaults.set(savedTruths, forKey: Keys.playerTruths)
        
        var savedDares = AppProvider.userDefaults.object(forKey: Keys.playerDares) as? [String : [Int]] ?? [String : [Int]]()
        for (playerId, score) in dareScores {
            if var savedScore = savedDares[playerId] {
                savedScore[0] += score[0]
                savedScore[1] += score[1]
                savedDares[playerId] = savedScore
            } else {
                savedDares[playerId] = [score[0], score[1]]
            }
        }
        AppProvider.userDefaults.set(savedDares, forKey: Keys.playerDares)
        
        self.performSegue(withIdentifier: "StatsViewController", sender: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        swipeableView.nextView = {
            return self.nextCardView()
        }
    }

    private func showQuitGameAlert() {
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "JosefinSans-SemiBold", size: 20)!,
            kTextFont: UIFont(name: "JosefinSans-SemiBold", size: 14)!,
            kButtonFont: UIFont(name: "JosefinSans-Bold", size: 14)!,
            showCloseButton: false
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton("Yes") {
            self.navigationController?.popViewController(animated: true)
        }
        alert.addButton("No") { }
        let subtitleText = customGame ? "Are you sure you want to quit? Your statistics will not be recorded." : "Are you sure you want to quit?"
        alert.showWarning("Warning", subTitle: subtitleText)
    }
    
    private func showFinishGameAlert() {
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "JosefinSans-SemiBold", size: 20)!,
            kTextFont: UIFont(name: "JosefinSans-SemiBold", size: 14)!,
            kButtonFont: UIFont(name: "JosefinSans-Bold", size: 14)!,
            showCloseButton: false
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton("Yes") {
            self.finishGameAndOpenStats()
        }
        alert.addButton("No") { }
        alert.showWarning("Warning", subTitle: "Are you sure you want to finish this game?")
    }
    
    private func addShadow(to view: UIView) {
        view.layer.shadowOffset = CGSize(width: 8.0, height: 8.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 4
        view.layer.shadowOpacity = 0.25
        view.layer.masksToBounds = false;
        view.clipsToBounds = false;
    }
    
    private func addRoundedCorners(to button: UIButton) {
        button.layer.cornerRadius = 5;
        button.layer.masksToBounds = true;
    }
    
    func nextCardView() -> UIView? {
        let cardView = CardView(frame: swipeableView.bounds)
        cardView.backgroundColor = UIColor.white
        
        contentView = Bundle.main.loadNibNamed("CardContentView", owner: self, options: nil)?.first! as! CardView
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.backgroundColor = cardView.backgroundColor
        contentView.layer.borderWidth = 5.0
        contentView.tdLabel.textDropShadow()
        contentView.personsLabel.textDropShadow()
        contentView.bgView.layer.cornerRadius = 10.0
        
        if customGame == true && gameStarted == true {
            let playerName = playerNames[totalNumberOfTruthsAndDaresDisplayed % playerNames.count]
            let playerId = playerIds[totalNumberOfTruthsAndDaresDisplayed % playerIds.count]
            contentView.currentPlayerId = playerId
            truthScoreValueLabel.text = "\(truthScores[playerId]?[0] ?? 0)" + "/" + "\(truthScores[playerId]?[1] ?? 0)"
            dareScoreValueLabel.text = "\(dareScores[playerId]?[0] ?? 0)" + "/" + "\(dareScores[playerId]?[1] ?? 0)"
            totalNumberOfTruthsAndDaresDisplayed += 1
            mainTitleLabel.text = playerName
        }
        
        if gameStarted == false {
            gameStarted = true
            contentView.initializeView(tdId: "", td: "", hideOverlay: false, challenge: Challenge.truth)
        } else if gameStarted == true {
            
            if customGame == true {
                truthScoreLabel.isHidden = false
                truthScoreValueLabel.isHidden = false
                dareScoreLabel.isHidden = false
                dareScoreValueLabel.isHidden = false
                finishButton.isHidden = false
            }
                
            switch challenge {
            case .truth:
                let t = randomTruth
                let tId = randomTruthId
                var savedWinners = AppProvider.userDefaults.object(forKey: Keys.truthWinners) as? [String : [String]] ?? [String : [String]]()
                var person: String?
                if let playerIds = savedWinners[tId] {
                    let randomIndex = Int(arc4random_uniform(UInt32(playerIds.count)))
                    let playerId  = playerIds[randomIndex]
                    if let p = playersDictionary[playerId] {
                        person = p
                    }
                }
                contentView.initializeView(tdId: tId, td: t, hideOverlay: true, challenge: Challenge.truth, person: person)
                contentView.layer.borderColor = UIColor(hexString: "689864")!.cgColor
                if customGame == true && displayState != .showCard {
                    updateButtons(firstButtonText: "Answered", secondButtonText: "Nope")
                }
            case .dare:
                let d = randomDare
                let dId = randomDareId
                var savedWinners = AppProvider.userDefaults.object(forKey: Keys.dareWinners) as? [String : [String]] ?? [String : [String]]()
                var person: String?
                if let playerIds = savedWinners[dId] {
                    let randomIndex = Int(arc4random_uniform(UInt32(playerIds.count)))
                    let playerId  = playerIds[randomIndex]
                    if let p = playersDictionary[playerId] {
                        person = p
                    }
                }
                contentView.initializeView(tdId: dId, td: d, hideOverlay: true, challenge: Challenge.dare, person: person)
                contentView.layer.borderColor = UIColor(hexString: "993303")!.cgColor
                if customGame == true && displayState != .showCard {
                    updateButtons(firstButtonText: "Done", secondButtonText: "Nope")
                }
            }
            displayState = .doIt
        }
        cardView.addSubview(contentView)
        
        constrain(contentView, cardView) { view1, view2 in
            view1.left == view2.left
            view1.top == view2.top
            view1.width == cardView.bounds.width
            view1.height == cardView.bounds.height
        }
        
        return cardView
    }

    private func updateButtons(firstButtonText: String, secondButtonText: String) {
        truthButton.alpha = 1.0
        truthButton.isEnabled = true
        truthButton.isUserInteractionEnabled = true
        dareButton.alpha = 1.0
        dareButton.isEnabled = true
        dareButton.isUserInteractionEnabled = true
        
        truthButton.setTitle(firstButtonText, for: UIControlState.normal)
        truthButton.setTitle(firstButtonText, for: UIControlState.selected)
        randomButton.isUserInteractionEnabled = false
        randomButton.isEnabled = false
        randomButton.alpha = 0.3
        dareButton.setTitle(secondButtonText, for: UIControlState.normal)
        dareButton.setTitle(secondButtonText, for: UIControlState.selected)
    }
    
    private func hideTDButtonsIfEmpty() {
        if noTruths == true {
            truthButton.alpha = 0.3
            truthButton.isUserInteractionEnabled = false
            truthButton.isEnabled = false
        } else {
            truthButton.alpha = 1.0
            truthButton.isUserInteractionEnabled = true
            truthButton.isEnabled = true
        }
        
        if noDares == true {
            dareButton.alpha = 0.3
            dareButton.isUserInteractionEnabled = false
            dareButton.isEnabled = false
        } else {
            dareButton.alpha = 1.0
            dareButton.isUserInteractionEnabled = true
            dareButton.isEnabled = true
        }
        
        if noTruths == true || noDares == true {
            randomButton.alpha = 0.3
            randomButton.isUserInteractionEnabled = false
            randomButton.isEnabled = false
        } else {
            randomButton.alpha = 1.0
            randomButton.isUserInteractionEnabled = true
            randomButton.isEnabled = true
        }
    }
    
    private func resetButtons(firstButtonText: String, secondButtonText: String) {
        hideTDButtonsIfEmpty()
        if noTruths == true || noDares == true {
            randomButton.isUserInteractionEnabled = false
            randomButton.isEnabled = false
            randomButton.alpha = 0.3
        } else {
            randomButton.isUserInteractionEnabled = true
            randomButton.isEnabled = true
            randomButton.alpha = 1.0
        }
        
        truthButton.setTitle(firstButtonText, for: UIControlState.normal)
        truthButton.setTitle(firstButtonText, for: UIControlState.selected)
        dareButton.setTitle(secondButtonText, for: UIControlState.normal)
        dareButton.setTitle(secondButtonText, for: UIControlState.selected)
    }
    
    @IBAction func truthAction(_ sender: Any) {
        if customGame == true {
            switch displayState {
            case .showCard:
                challenge = Challenge.truth
                displayState = .doIt
                swipeableView.swipeTopView(inDirection: .Left)
            case .doIt:
                switch challenge {
                case .truth:
                    if var score = truthScores[contentView.currentPlayerId] {
                        score[1] += 1
                        score[0] += 1
                        truthScores[contentView.currentPlayerId] = score
                        var savedWinners = AppProvider.userDefaults.object(forKey: Keys.truthWinners) as? [String : [String]] ?? [String : [String]]()
                        if var ws = savedWinners[contentView.currentTDId] {
                            ws.append(contentView.currentPlayerId)
                            savedWinners[contentView.currentTDId] = ws
                        } else {
                            savedWinners[contentView.currentTDId] = [contentView.currentPlayerId]
                        }
                        AppProvider.userDefaults.set(savedWinners, forKey: Keys.truthWinners)
                        
                        Answers.logCustomEvent(withName: "Successful Truth Completion",
                                               customAttributes: [
                                                "Truth": contentView.td,
                                                "Player": (mainTitleLabel.text ?? "")
                                            ])
                    }
                case .dare:
                    if var score = dareScores[contentView.currentPlayerId] {
                        score[1] += 1
                        score[0] += 1
                        dareScores[contentView.currentPlayerId] = score
                        var savedWinners = AppProvider.userDefaults.object(forKey: Keys.dareWinners) as? [String : [String]] ?? [String : [String]]()
                        if var ws = savedWinners[contentView.currentTDId] {
                            ws.append(contentView.currentPlayerId)
                            savedWinners[contentView.currentTDId] = ws
                        } else {
                            savedWinners[contentView.currentTDId] = [contentView.currentPlayerId]
                        }
                        AppProvider.userDefaults.set(savedWinners, forKey: Keys.dareWinners)
                        Answers.logCustomEvent(withName: "Successful Dare Completion",
                                               customAttributes: [
                                                "Dare": contentView.td,
                                                "Player": (mainTitleLabel.text ?? "")
                            ])
                    }
                }
                
                let playerId = contentView.currentPlayerId
                truthScoreValueLabel.text = "\(truthScores[playerId]?[0] ?? 0)" + "/" + "\(truthScores[playerId]?[1] ?? 0)"
                dareScoreValueLabel.text = "\(dareScores[playerId]?[0] ?? 0)" + "/" + "\(dareScores[playerId]?[1] ?? 0)"
                
                contentView.setupFeedback(done: true, nextPlayer: playerNames[totalNumberOfTruthsAndDaresDisplayed % playerNames.count])
                resetButtons(firstButtonText: "Truth", secondButtonText: "Dare")
                displayState = .showCard
            }
        } else {
            challenge = Challenge.truth
            swipeableView.swipeTopView(inDirection: .Left)
        }
    }
    
    @IBAction func randomAction(_ sender: Any) {
        let randomNumber = randomBetween(min: 1, max: 3)
        if randomNumber == 1 {
            if customGame == true {
                switch displayState {
                case .showCard:
                    challenge = Challenge.truth
                    displayState = .doIt
                    swipeableView.swipeTopView(inDirection: .Up)
                case .doIt:
                    contentView.setupFeedback(done: true, nextPlayer: playerNames[totalNumberOfTruthsAndDaresDisplayed % playerNames.count])
                    resetButtons(firstButtonText: "Truth", secondButtonText: "Dare")
                    displayState = .showCard
                }
            } else {
                challenge = Challenge.truth
                swipeableView.swipeTopView(inDirection: .Up)
            }
        } else {
            if customGame == true {
                switch displayState {
                case .showCard:
                    displayState = .doIt
                    challenge = Challenge.dare
                    swipeableView.swipeTopView(inDirection: .Up)
                case .doIt:
                    contentView.setupFeedback(done: false, nextPlayer: playerNames[totalNumberOfTruthsAndDaresDisplayed % playerNames.count])
                    resetButtons(firstButtonText: "Truth", secondButtonText: "Dare")
                    displayState = .showCard
                }
            } else {
                challenge = Challenge.dare
                swipeableView.swipeTopView(inDirection: .Up)
            }
        }
        swipeableView.swipeTopView(inDirection: .Up)
    }
    
    @IBAction func dareAction(_ sender: Any) {
        if customGame == true {
            switch displayState {
            case .showCard:
                displayState = .doIt
                challenge = Challenge.dare
                swipeableView.swipeTopView(inDirection: .Right)
            case .doIt:
                switch challenge {
                case .truth:
                    if var score = truthScores[contentView.currentPlayerId] {
                        score[1] += 1
                        truthScores[contentView.currentPlayerId] = score
                        
                        Answers.logCustomEvent(withName: "Truth Failure",
                                               customAttributes: [
                                                "Dare": contentView.td,
                                                "Player": (mainTitleLabel.text ?? "")
                            ])
                    }
                case .dare:
                    if var score = dareScores[contentView.currentPlayerId] {
                        score[1] += 1
                        dareScores[contentView.currentPlayerId] = score
                        
                        Answers.logCustomEvent(withName: "Dare Failure",
                                               customAttributes: [
                                                "Dare": contentView.td,
                                                "Player": (mainTitleLabel.text ?? "")
                            ])
                    }
                }
                let playerId = contentView.currentPlayerId
                truthScoreValueLabel.text = "\(truthScores[playerId]?[0] ?? 0)" + "/" + "\(truthScores[playerId]?[1] ?? 0)"
                dareScoreValueLabel.text = "\(dareScores[playerId]?[0] ?? 0)" + "/" + "\(dareScores[playerId]?[1] ?? 0)"
                contentView.setupFeedback(done: false, nextPlayer: playerNames[totalNumberOfTruthsAndDaresDisplayed % playerNames.count])
                resetButtons(firstButtonText: "Truth", secondButtonText: "Dare")
                displayState = .showCard
            }
        } else {
            challenge = Challenge.dare
            swipeableView.swipeTopView(inDirection: .Right)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "StatsViewController":
            let vc = segue.destination as! StatsViewController
            vc.truthScores = self.truthScores
            vc.dareScores = self.dareScores
            vc.gameOn = true
            var players = ""
            for name in playerNames {
                players += "\(name) "
            }
            Answers.logCustomEvent(withName: "Game Done", customAttributes: ["Player Names" : players])
        default:
            return
        }
    }
}

extension TDViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

extension UILabel {
    func textDropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
    }
}
